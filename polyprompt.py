#!/usr/bin/env python3
"""
Prints a random character that can be used in your prompt instead of '$'
"""

from random import choice as rchoice

characters = ["£", "€", "➤", "➫", "↺", 
              "🤖", "🎅", "🖖", "💨", "🤞",
              "🎮", "🎱", "⚓", "🌄", "🎠",
              "🎭", "🚽", "🛸", "🍆", "🍑",
              "🍺", "🍵", "🥐", "🥦", "🥧",
              "🌺", "🐁", "🌼", "🐧", "🐨",
              "🦕", "💬", "💾"]

selected_char = rchoice(characters)

print(selected_char)

